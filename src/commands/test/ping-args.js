module.exports = {
	name: 'pingargs',
	description: 'Ping! but with argument',
	execute(message, args) {
		message.channel.send(`Pong\nArgument Count: ${args.length}\nArguments: ${args}`);
	},
};