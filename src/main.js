require('dotenv').config();
const Discord = require('discord.js');
const fs = require('fs');
const { prefix } = require('../config.json');

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandDirectories = fs.readdirSync('./src/commands');
for(const directory of commandDirectories) {
    try {
        const commands = fs.readdirSync(`./src/commands/${directory}`)
            .filter(file => file.endsWith('.js'));

        for(const commandName of commands) {
            const command = require(`./commands/${directory}/${commandName}`);
            client.commands.set(command.name, command);
        }
    } catch (error) {
        console.log(process.cwd());
        console.error(error);
    }
}

client.login(process.env.BOT_TOKEN);

client.on('ready', () => {
    console.log('Bot Online');
});

client.on('message', message => {
    if(!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(/ +/);
    const command = args.shift().toLowerCase();

    if(!client.commands.has(command)) return;

    try {
        client.commands.get(command).execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply('There was an error trying to execute that command');
    }
});